export default function Configuration (size, color, delay = 30) {
    this.size = size
    this.color = color
    this.delay = delay
}
