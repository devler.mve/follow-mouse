import Vue from 'vue'
import {EventBus} from "../modules/EventBus";

export default Vue.directive('hover', {
    bind(el) {
        if (el) {
            el.addEventListener('mouseover', () => {
                EventBus.$emit('follow-mouse-over')
            })
            el.addEventListener('mouseleave', () => {
                EventBus.$emit('follow-mouse-leave')
            })
        }
    },
    unbind() {
        EventBus.$emit('follow-mouse-leave')
    }
})
